## Description
Ce projet met à disposition dans vos projets des builder vous permettant de créer plusieurs objets différents.
Actuellement, il permet de créer les objets suivants :
* variant
* uuid

### Création d'un variant
Vous pouvez créer des variants sans utiliser la méthode de WinDev, que ce soit la création d'un variant et de spécifier ligne après ligne chaque valeur du variant, soit depuis un fichiers json avec la méthode `JSonToVariant`.

#### L'objectif
L'intérêt d'utiliser ce builder est de standardiser la création d'un variant et de pouvoir l'écrire en une seule ligne. Cela peut être très utile pour rendre les tests beaucoup plus simple à lire.
Cet outil permet aussi de créer des variants dynamiquement.

#### Exemples
##### Exemple brut

```
v is variant
v.text = "some text"
v.'other text' = "other text"
v.values[1] = 10
v.values[2] = 20
v.values[3] = 30
v.items["a"] = "A"
v.items["b"] = "B"
```

est équivalent à la syntaxe suivante :
```
cVariantBuilder.create()...
    .with_sub_value("text", "some text")...
    .with_sub_value("other text", "other text")...
    .with_sub_value("values", cVariantBuilder.create()...
                                .with_value(10)...
                                .with_value(20)...
                                .with_value(30))...
    .with_sub_value("items", cVariantBuilder.create()...
                                    .with_item("a", "A")...
                                    .with_item("b", "B"))...
    .build()
```

##### Avec une méthode masquant la création du builder

On peut aussi créer la procédure `a_variant()` qui masque la création du builder()

```
a_variant()...
    .with_sub_value("text", "some text")...
    .with_sub_value("other text", "other text")...
    .with_sub_value("values", a_variant()...
                                .with_value(10)...
                                .with_value(20)...
                                .with_value(30))...
    .with_sub_value("items", a_variant()...
                                    .with_item("a", "A")...
                                    .with_item("b", "B"))...
    .build()
```

##### En une seule ligne
```
a_variant().with_sub_value("text", "some text").with_sub_value("other text", "other text").with_sub_value("values", a_variant().with_value(10).with_value(20).with_value(30)).with_sub_value("items", a_variant().with_item("a", "A").with_item("b", "B")).build()
```

### Création d'un uuid
Vous pouvez créer un variant en une seule ligne, à partir d'une chaine de caractère, sans avoir besoin de passer par une variable intermédiaire.

#### Exemple
##### Avec le builder
```
uuid_builder is cUuidBuilder dynamic = cUuidBuilder.create()
actual is UUID = uuid_builder.from_string("1965579c-16ee-46b7-a7c4-1647b0bde165").build()
expected is string = "1965579c-16ee-46b7-a7c4-1647b0bde165"

assert_equal(expected, actual)
```

##### Avec une méthode masquant la création du builder

```
actual is UUID = an_uuid().from_string("1965579c-16ee-46b7-a7c4-1647b0bde164").build()
expected is UUID = "1965579c-16ee-46b7-a7c4-1647b0bde164"

assert_equal(expected, actual)
```